from distutils.core import setup

setup(name='python-libguess',
      version='1.1',
      description='Python wrapper for libguess.',
      author='Jussi Judin',
      author_email='jjudin+python-libguess@iki.fi',
      py_modules=['guess'],
      url="https://bitbucket.org/barro/python-libguess/"
      )
